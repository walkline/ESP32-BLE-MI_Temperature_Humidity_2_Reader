"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/ESP32-BLE-MI_Temperature_Humidity_2_Reader
"""
# MI BLE IoT device data reader

import ubluetooth as bt
from utime import sleep
from ble.tools import BLETools
from ble.const import BLEConst
from ble.miot_factory import MIOTFactory
import gc


class MIOTException(Exception):
	pass


class BLEMIOT(object):
	def __init__(self, ble, scan_completed_cb=None):
		self.__ble = ble
		self.__factory = MIOTFactory()
		self.__scan_completed_cb = scan_completed_cb

		self.__ble.active(False)
		print("activating ble...")
		self.__ble.active(True)
		print("ble activated")

		self.__ble.config(rxbuf=256)
		self.__ble.irq(self.__irq)

	def scan(self, senconds=20):
		self.__factory.clear()
		print("scaning for {} second(s)...".format(senconds))
		self.__ble.gap_scan(senconds * 1000, 50000, 50000)

	def __irq(self, event, data):
		if event == BLEConst.IRQ.IRQ_SCAN_RESULT:
			# addr_type, addr, adv_type, rssi, adv_data = data
			if MIOTFactory.check(data[4]):
				self.__factory.append(data)
		elif event == BLEConst.IRQ.IRQ_SCAN_DONE:
			print("scan completed, {} MIOT device(s) found".format(self.__factory.count()))
			
			if self.__scan_completed_cb is not None:
				self.__scan_completed_cb(self.__factory.devices())
		elif event == BLEConst.IRQ.IRQ_PERIPHERAL_CONNECT:
			# conn_handle, addr_type, addr = data
			device = self.__factory.find(data[2])

			if device:
				print("[{}] connected".format(device.name))
				device.conn_handle = data[0]
				self.__ble.gattc_discover_services(device.conn_handle)
		elif event == BLEConst.IRQ.IRQ_PERIPHERAL_DISCONNECT:
			# conn_handle, addr_type, addr = data
			device = self.__factory.find(data[2])

			if device:
				print("[{}] disconnected".format(device.name))
				device.conn_handle = None
		elif event == BLEConst.IRQ.IRQ_GATTC_NOTIFY:
			# conn_handle, value_handle, notify_data = data
			device = self.__factory.find(conn_handle=data[0])

			if device:
				temp, humi = self.__convert_temp_and_humi(data[2])
				print("[{}] notify: temp={}C, humi={}%".format(device.name, temp, humi))
		elif event == BLEConst.IRQ.IRQ_GATTC_SERVICE_RESULT:
			pass
		else:
			print("event: {}, data: {}".format(event, data))
		
		gc.collect()

	def connect_to_device(self, device):
		self.__ble.gap_connect(device.addr_type, device.addr)
	
	def __convert_temp_and_humi(self, value):
		assert isinstance(value, (bytes, memoryview)) and len(value) == 5, MIOTException("temp & humi value error")

		temp = int.from_bytes(value[:2], "little", False) / 100
		# humi = int.from_bytes(value[3:], "little", False) / 100
		humi = value[2]

		return temp, humi


def main():
	from machine import Pin

	def button_click_cb(timer):
		miot.scan()

	def scan_completed_cb(devices):
		for device in devices:
			if device.adv_type in (BLEConst.ADVType.ADV_DIRECT_IND, BLEConst.ADVType.ADV_IND):
				print("connecting to [{}]".format(device.name))
				miot.connect_to_device(device)
				break

	ble = bt.BLE()
	miot = BLEMIOT(ble, scan_completed_cb)

	button = Pin(0, Pin.IN, Pin.PULL_UP)
	button.irq(button_click_cb, Pin.IRQ_RISING)
	print("button initialized")

	miot.scan(5)


if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")
